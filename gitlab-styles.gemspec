# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab/styles/version'

Gem::Specification.new do |spec|
  spec.required_ruby_version = '>= 2.6'
  spec.name          = 'gitlab-styles'
  spec.version       = Gitlab::Styles::VERSION
  spec.authors       = ['GitLab']
  spec.email         = ['gitlab_rubygems@gitlab.com']

  spec.summary       = 'GitLab style guides and shared style configs.'
  spec.homepage      = 'https://gitlab.com/gitlab-org/ruby/gems/gitlab-styles'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(docs|test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'rubocop', '~> 0.91', '>= 0.91.1'
  spec.add_dependency 'rubocop-gitlab-security', '~> 0.1.1'
  spec.add_dependency 'rubocop-graphql', '~> 0.10'
  spec.add_dependency 'rubocop-performance', '~> 1.9.2'
  spec.add_dependency 'rubocop-rails', '~> 2.9'
  spec.add_dependency 'rubocop-rspec', '~> 1.44'

  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'gitlab-dangerfiles', '~> 2.6.1'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
