# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../../lib/gitlab/styles/rubocop/cop/active_record_dependent'

RSpec.describe Gitlab::Styles::Rubocop::Cop::ActiveRecordDependent do
  subject(:cop) { described_class.new }

  context 'when inside the app/models directory' do
    it 'registers an offense when dependent: is used' do
      allow(cop).to receive(:in_model?).and_return(true)

      expect_offense(<<~RUBY)
        belongs_to :foo, dependent: :destroy
                         ^^^^^^^^^^^^^^^^^^^ Do not use `dependent: to remove associated data, use foreign keys with cascading deletes instead
      RUBY
    end
  end

  context 'when outside the app/models directory' do
    it 'does nothing' do
      allow(cop).to receive(:in_model?).and_return(false)

      expect_no_offenses(<<~RUBY)
        belongs_to :foo, dependent: :destroy
      RUBY
    end
  end
end
