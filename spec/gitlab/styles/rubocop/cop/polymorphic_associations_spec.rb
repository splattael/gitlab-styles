# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../../lib/gitlab/styles/rubocop/cop/polymorphic_associations'

RSpec.describe Gitlab::Styles::Rubocop::Cop::PolymorphicAssociations do
  subject(:cop) { described_class.new }

  context 'when inside the app/models directory' do
    it 'registers an offense when polymorphic: true is used', :aggregate_failures do
      allow(cop).to receive(:in_model?).and_return(true)

      expect_offense(<<~RUBY)
        belongs_to :foo, polymorphic: true
                         ^^^^^^^^^^^^^^^^^ Do not use polymorphic associations, use separate tables instead
      RUBY
    end
  end

  context 'when outside the app/models directory' do
    it 'does nothing' do
      allow(cop).to receive(:in_model?).and_return(false)

      expect_no_offenses(<<~RUBY)
        belongs_to :foo, polymorphic: true
      RUBY
    end
  end
end
