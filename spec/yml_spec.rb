# frozen_string_literal: true

require "spec_helper"
require "open3"

RSpec.describe "provided .yml files" do
  Dir["rubocop-*.yml"].each do |yml|
    it "runs #{yml} successfully without other files", :aggregate_failures do
      _output, error, status = Open3.capture3("bundle exec rubocop -c #{yml}")

      expect(error).to be_empty
      expect(status.exitstatus).not_to eq(2)
    end
  end
end
