# frozen_string_literal: true

require_relative '../../model_helpers'

module Gitlab
  module Styles
    module Rubocop
      module Cop
        module Rails
          class IncludeUrlHelper < RuboCop::Cop::Cop
            MSG = <<~MSG
              Avoid including `ActionView::Helpers::UrlHelper`.
              It adds/overrides ~40 methods while usually only one is needed.
              Instead, use the `Gitlab::Routing.url_helpers`/`Application.routes.url_helpers`(outside of gitlab)
              and `ActionController::Base.helpers.link_to`.
              See https://gitlab.com/gitlab-org/gitlab/-/issues/340567.
            MSG

            def_node_matcher :include_url_helpers_node?, <<~PATTERN
              (send nil? :include (const (const (const {nil? cbase} :ActionView) :Helpers) :UrlHelper))
            PATTERN

            def on_send(node)
              add_offense(node) if include_url_helpers_node?(node)
            end
          end
        end
      end
    end
  end
end
