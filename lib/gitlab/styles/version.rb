# frozen_string_literal: true

module Gitlab
  module Styles
    VERSION = '6.6.0'
  end
end
