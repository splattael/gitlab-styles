<!-- Replace `<NEW_VERSION>` with the previous release here, and `<COMMIT_UPDATING_VERSION>`
with the latest commit from this merge request. -->
- Diff: https://gitlab.com/gitlab-org/ruby/gems/gitlab-styles/compare/v<NEW_VERSION>...<COMMIT_UPDATING_VERSION>

- Checklist before merging:
  - [ ] Diff link is up-to-date.
  - [ ] Based on the diff, `version.rb` is updated, according to [SemVer](https://semver.org).

- Checklist after merging:
  - [ ] Check that automatic release notes (generated following the same process as https://docs.gitlab.com/ee/development/changelog.html) are correct.

/label ~"type::maintenance" ~"static code analysis"
